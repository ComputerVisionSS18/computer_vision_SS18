# We use the DIBR algorithm

Tasks and basic algorithm in google docs: https://docs.google.com/document/d/1GSgy19oaPks-Os82Xxh8d8jrhIVMvnuc0KcqmgqpTAA/edit?usp=sharing


## Kalibration

## Correspondences

## Epipolar geometry

    - Rotation, Translation, Essential/Fundamental 
    
## Depth Map

    - [Rectification ](http://www.diegm.uniud.it/fusiello/papers/mva99.pdf)
    - [Disparity Reference Implementation](https://github.com/mbaird/stereo-disparity-map)
    - Rectified Depth 
    - Derectify
    
## Rendering

## Post Processing
% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3304.111897759421936 ; 3311.117141576638005 ];

%-- Principal point:
cc = [ 1509.165565816276057 ; 945.919052846839122 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.007876230068403 ; 0.078223215068360 ; 0.001381906957542 ; -0.000363975857129 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 82.599241128620122 ; 81.675148405632598 ];

%-- Principal point uncertainty:
cc_error = [ 23.372721948391472 ; 23.669298831556535 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.020426831862522 ; 0.143978132800769 ; 0.001726992429423 ; 0.002287132543670 ; 0.000000000000000 ];

%-- Image size:
nx = 3000;
ny = 2000;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 6;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.825127e+00 ; 2.513893e+00 ; -1.350758e-01 ];
Tc_1  = [ -6.076068e+01 ; -3.062255e+02 ; 1.545675e+03 ];
omc_error_1 = [ 5.140000e-03 ; 8.606980e-03 ; 1.592947e-02 ];
Tc_error_1  = [ 1.102918e+01 ; 1.112662e+01 ; 3.770233e+01 ];

%-- Image #2:
omc_2 = [ -2.139757e+00 ; -2.162772e+00 ; 1.621535e-02 ];
Tc_2  = [ -4.183881e+01 ; -4.566358e+01 ; 9.874897e+02 ];
omc_error_2 = [ 6.587402e-03 ; 7.342201e-03 ; 1.341656e-02 ];
Tc_error_2  = [ 6.986714e+00 ; 7.064079e+00 ; 2.443054e+01 ];

%-- Image #3:
omc_3 = [ -1.871607e+00 ; 1.882753e+00 ; -4.884257e-01 ];
Tc_3  = [ -2.122945e+02 ; 6.772228e+01 ; 1.085982e+03 ];
omc_error_3 = [ 6.482373e-03 ; 6.644180e-03 ; 1.609660e-02 ];
Tc_error_3  = [ 7.742374e+00 ; 7.807610e+00 ; 2.498588e+01 ];

%-- Image #4:
omc_4 = [ 2.173397e+00 ; 2.225780e+00 ; 1.601054e-01 ];
Tc_4  = [ -1.308308e+02 ; -6.431527e+01 ; 5.152205e+02 ];
omc_error_4 = [ 4.143724e-03 ; 3.871724e-03 ; 9.599108e-03 ];
Tc_error_4  = [ 3.679468e+00 ; 3.731996e+00 ; 1.262678e+01 ];

%-- Image #5:
omc_5 = [ NaN ; NaN ; NaN ];
Tc_5  = [ NaN ; NaN ; NaN ];
omc_error_5 = [ NaN ; NaN ; NaN ];
Tc_error_5  = [ NaN ; NaN ; NaN ];

%-- Image #6:
omc_6 = [ -2.067098e+00 ; -2.201164e+00 ; -2.509756e-02 ];
Tc_6  = [ -1.483189e+02 ; -9.780231e+01 ; 5.032991e+02 ];
omc_error_6 = [ 4.393751e-03 ; 4.235624e-03 ; 8.354254e-03 ];
Tc_error_6  = [ 3.564921e+00 ; 3.657683e+00 ; 1.244683e+01 ];


function C = getCorrespondenceMatrix(img1,img2,f1,f2,window_length,min_corr);
% Find correspondences
% img1/2: Grey images
% f1/2: features of the images
% window_length: window around features, which is used for comparison
% min_corr: minimal corresponding value
 
    %% Preparation
    % Remove all points that are too close to the image border
    [~, f1] = filterBorder(img1, f1, window_length);
    [~, f2] = filterBorder(img2, f2, window_length);
   
    % Norm window patches and stack them
    Mat_feat_1 = get_feature_windows(img1, f1, window_length);
    Mat_feat_2 = get_feature_windows(img2, f2, window_length);

    c = 1/(window_length^2-1);
    
    %% NCC Brechnung
    NCC_matrix = zeros([length(f1), length(f2)]);
    for i_1 = 1:length(f1)
        for i_2 = 1:length(f2)
            NCC_matrix(i_1, i_2) = c * trace(Mat_feat_1(:, i_1)' * Mat_feat_2(:, i_2));
        end
    end

    NCC_matrix(NCC_matrix < min_corr) = 0;
    
    NCC_matrix = NCC_matrix';
    
    [sorted_values, sorted_index] = sort(NCC_matrix(:), 'descend');
    sorted_index = sorted_index(sorted_values > 0);
    
    %% Create the correspondence matrix
    C = [];
    for idx = sorted_index'
        % skip if already included
        if NCC_matrix(idx) == 0
            continue;
        end

        % get reasonable coordinates
        [x, y] = idx2coord(idx, size(NCC_matrix, 1));

        % mark feature point for skipping
        NCC_matrix(:, x) = 0;
		NCC_matrix(y, :) = 0;

        % add feature points
        C = [C, [f1(:, x); f2(:, y)]];
    end    
end



function[num_pts Mpt_new] = filterBorder(img, Mpt, window_length)
    double_bild = double(img);
    [x, y] = size(double_bild);

    randbereich = ones([x y]);
    randbereich(1:((window_length-1)/2),1:y)= zeros;
    randbereich(1:x, 1:((window_length-1)/2))= zeros;
    randbereich(x-((window_length-1)/2):x, 1:y)= zeros;
    randbereich(1:x, y-((window_length-1)/2):y)= zeros;

    Mpt_new = [];
    for i = 1:length(Mpt)
        y = Mpt(1, i);
        x= Mpt(2, i);
        if randbereich(x,y) ~= 0
            Mpt_new = [ Mpt_new [y;x]];
        end
    end

    num_pts = length(Mpt_new);
end

function[Mat_feat] = get_feature_windows(img, Mpt, window_length)
   I_double = double(img);

   Mat_feat = [];
   for i = 1:length(Mpt)
      y = Mpt(1, i);
      x = Mpt(2, i);
      w_h = (window_length - 1)/2;
      feature_window = I_double(floor(x-w_h) : floor(x+w_h), floor(y-w_h) : floor(y+w_h));
      feature_window = norm_w(feature_window);
      feature_window = reshape(feature_window, [window_length^2,1]);
      Mat_feat = [Mat_feat feature_window];
   end 
end

function[new_window] = norm_w(window)
    % mu = numel(window) .* window            % Mittelwert
    average_value = sum(window(:)) / numel(window);
    win_mu= ones(size(window)) * average_value;
    % sigma = (1/numel(window).*)  
    sigma_window = std(window(:));
    
    new_window = (window - win_mu) / sigma_window;
end

function [x, y] = idx2coord(idx, rows)
    % convert index to x, y representation
    x = ceil(idx/rows);
    y = mod(idx - 1, rows) + 1;
end
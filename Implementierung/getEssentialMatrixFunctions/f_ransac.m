function [robust_C] = f_ransac(C, epsilon, p, tolerance)
% C: current corresponding coordinates
% epsilon: chance a random point is an outlier
% p: expected probability that chosen, robust points contain no outliers
% tolerance: tolerance, that a pair of corresponding points fits to the to be computed model

    %% Transform into pixelcoordinates
    % simply take x_i and y_i and add 3rd dimension = 1
    n = length(C);
    x1_pixel = [C(1:2, :); ones(1, n)];
    x2_pixel = [C(3:4, :); ones(1, n)];
    
    %% RANSAC preparations
    k = 8;
    s = log(1 - p)/(log(1 - (1 - epsilon)^k));
    largest_set_size = 0;
    largest_set_dist = Inf;
    largest_set_F = zeros(3, 3);
    
    %% Run RANSAC
    robust_C = C;
    for i = 1:s
        % Get k random positions
        cols = sort(randperm(length(C), k));

        % Calculate Fundamentalmatrix
        F = achtPunktAlgorithmus(C(:, cols));

        % Check sampson distance and remove pairs with bad tolerance
        sd = sampson_dist(F, x1_pixel, x2_pixel);
        sd(sd >= tolerance) = 0;

        % Count size and sampson distance
        set_size = length(sd(sd ~= 0));
        set_dist = sum(sd);

        % Check if choosen Fundamentalmatrix is the best one
        if largest_set_size < set_size || (largest_set_size == set_size && largest_set_dist > set_dist)
            largest_set_F = F;
            largest_set_size = set_size;
            largest_set_dist = set_dist;

            robust_C = C(:, sd ~= 0);
        end
    end
end

function sd = sampson_dist(F, x1_pixel, x2_pixel)
    % Diese Funktion berechnet die Sampson Distanz basierend auf der
    % Fundamentalmatrix F

    % Get skew-symmetric
    e_hat = skew([0, 0, 1]);

    % get nominator
    Fy = (x2_pixel')*F;
    numerator = (sum((Fy').*x1_pixel)).^2;

    % get denominator summands
    denominator_1 = sum((e_hat*F*x1_pixel).^2);
    denominator_2 = sum((x2_pixel'*F*e_hat)'.^2);

    sd = numerator ./ (denominator_1 + denominator_2);

end

function X = skew(x)
    X = [ 0 -x(3) x(2) ; x(3) 0 -x(1) ; -x(2) x(1) 0 ];
end
function [Ix, Iy] = sobel_filter(input_image)
    % In dieser Funktion soll das Sobel-Filter implementiert werden, welches
    % ein Graustufenbild einliest und den Bildgradienten in x- sowie in
    % y-Richtung zurueckgibt.
    G_x = [1 2 1]' * [1 0 -1];
    G_y = ([1 2 1]' * [1 0 -1])';

    Fx = conv2(G_x, input_image);
    Fy = conv2(G_y, input_image);
    
    Ix = Fx(2:end-1, 2:end-1);
    Iy = Fy(2:end-1, 2:end-1);
end

function [Ix, Iy] = sobel_filter_large(input_image)
    % Different to the last one, this function uses a 5x5 kernel
    G_x = [1 2 3 2 1]' * [1 1 0 -1 -1];
    G_y = ([1 2 3 2 1]' * [1 1 0 -1 -1])';

    Fx = conv2(G_x, input_image);
    Fy = conv2(G_y, input_image);
    
    Ix = Fx(2:end-1, 2:end-1);
    Iy = Fy(2:end-1, 2:end-1);
end
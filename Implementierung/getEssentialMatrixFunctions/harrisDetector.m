function features = harrisDetector(input_img, segment_length, k, tau, min_dist, tile_size, N)
    % expects a gray image
    % segment_length: size of a segment
    % k: corner and edge priority
    % tau: threshold to detect a corner
    % min_dist: minimal distance between two features
    % tile_size: size of a kachel
    % N: maximum number of detected features

    %% Preparation
    % Prepare weights (gaussian)
    w = gaussian1d(segment_length, 1);

    % Apply gaussian filter
    N_gauss = 5; %// Define size of Gaussian mask
    sigma = 0.3; %// Define sigma here
    
    input_img_blurred = gaussian_filter(input_img, N_gauss, sigma);
    
    % Apply the sobel filter to find gradients
    [Ix, Iy] = sobel_filter(input_img_blurred);

   % figure; imshow((Ix + Iy) ./ 2);
    
    
    %% Calculate harris matrix G
    Ix2 = Ix.* Ix;
    Iy2 = Iy.* Iy;
    Ixy = Ix .* Iy;
    
    % Use weights
    G11 = conv2(w,w,Ix2,'same');
    G22 = conv2(w,w,Iy2,'same');
    G12 = conv2(w,w,Ixy,'same');
    
    
    %% Extract features through H matrix
    % H = det(G) - k * trace(G)^2;
    H = (G11.*G22 - G12.^2) - k * (G11 + G22).^2;
    
    % Set the padding of H to zeros
    d = ceil(segment_length/2);
    corners = zeros(size(H));
    corners(1+d:end-d,1+d:end-d) = H(1+d:end-d,1+d:end-d);
    % Remove small values
    corners(corners<=tau)=0;
    
    % Insert border
    sizeC = size(corners);
    cornersPadded = zeros([sizeC(1)+2*min_dist,sizeC(2)+2*min_dist]);
    cornersPadded(1+min_dist:end-min_dist,1+min_dist:end-min_dist) = corners;
    
    % Sort features
    [out,sorted_index] = sort(cornersPadded(:), 'descend');
    X = find(~out);
    
    % Remove all features with a value of 0
    for idx = X
        sorted_index(idx) = [];
    end
    
    % Add an accumulator field to count the number of features per tile
    [r, c] = size(input_img);
    r = ceil(r/tile_size(1));
    c = ceil(c/tile_size(2));
    AKKA = zeros(r,c);
    
    corners = cornersPadded;
    % Reduce features that are too close together
    rows = size(corners, 1);
    
    Cake = cake(min_dist);
    features = [];
    for el = sorted_index'
        if corners(el) ~= 0
            % Get x and y position from index
            x = ceil(el/rows);
            y = mod(el - 1, rows) + 1;
            
            % Check if merkmal at position is allowed
            x_md = x - min_dist; 
            y_md = y - min_dist;
            x_akka = ceil(x_md/tile_size(1));
            y_akka = ceil(y_md/tile_size(2));
            if AKKA(y_akka, x_akka) ~= N-1
                % Add new merkmal to final merkmale and update AKKA
                corners(y-min_dist:y+min_dist, x-min_dist:x+min_dist) = corners(y-min_dist:y+min_dist, x-min_dist:x+min_dist).*Cake;
                AKKA(y_akka, x_akka) = AKKA(y_akka, x_akka) + 1;
                newMerkmal = [x_md; y_md];
                features = [features newMerkmal];
            end
        end
    end
end


function Cake = cake(min_dist)
    % Die Funktion cake erstellt eine "Kuchenmatrix", die eine kreisfoermige
    % Anordnung von Nullen beinhaltet und den Rest der Matrix mit Einsen
    % auffuellt. Damit koennen, ausgehend vom staerksten Merkmal, andere Punkte
    % unterdrueckt werden, die den Mindestabstand hierzu nicht einhalten. 
    Cake = ones(min_dist*2+1);
    for x = 1:min_dist*2+1
        for y = 1:min_dist*2+1
            % display([x,y,sqrt((x-min_dist-1)^2 + (y-min_dist-1)^2)]);
            if sqrt((x-min_dist-1)^2 + (y-min_dist-1)^2) <= min_dist
                Cake(x,y) = 0;
            else
                Cake(x,y) = 1;
            end
        end
    end
    Cake = logical(Cake);
end


function y=gaussian1d(N,sigma)
    x = linspace(-N / 2, N / 2, N);
    y = exp(-x .^ 2 / (2 * sigma ^ 2));
    y = y / sum (y); % normalize
end

function f=gaussian2d(N,sigma)
    N = N - 1;
    [x y]=meshgrid(floor(-N/2):floor(N/2), floor(-N/2):floor(N/2));
    f=exp(-x.^2/(2*sigma^2)-y.^2/(2*sigma^2));
    f=f./sum(f(:));
end
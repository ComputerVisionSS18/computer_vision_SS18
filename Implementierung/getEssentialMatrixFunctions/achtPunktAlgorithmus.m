function [EF] = achtpunktalgorithmus(C, K)
    % Diese Funktion berechnet die Essentielle Matrix oder Fundamentalmatrix
    % mittels 8-Punkt-Algorithmus, je nachdem, ob die Kalibrierungsmatrix 'K'
    % vorliegt oder nicht
    % C: correspondences
    % K: calibration matrix    
    %% Anfang Achtpunktalgorithmus aus Aufgabe 3.1
      
    [~,m] = size(C);
    
    %Separate KorrespondenzArray
    X1 = C(1:2,:);
    X2 = C(3:4,:);
    
    %Make homogene by addinf row of ones
    X1 = [X1;ones(1,m)];
    X2 = [X2;ones(1,m)];
    
    %If calibration ncessary calibrate with x = K^-1*x'
    if nargin > 1
       
        Ik = inv(K);
        X1 = Ik * X1;
        X2 = Ik * X2;
        
    end
    
    %Declare A
    A = [];
    
    %Calculate kroeneckerproduct for each vector
    for Iter = 1:m
        
        k = kron(X1(:,Iter),X2(:,Iter));
        A(end + 1,:) = [k'];
        
    end
    
    [~,~,V] = svd(A);
    
    %% Schaetzung der Matrizen
    
    G = [ V(1:3,9) V(4:6,9) V(7:9,9)];
    
    [U,E,V] = svd(G);
    
    Ee = [1 0 0;
          0 1 0;
          0 0 0];
      
    E(3,3) = 0;
    
    if nargin > 1
    
        EF = U * Ee * V';
        
    else
        
        EF = U * E * V';
        
    end
   
end
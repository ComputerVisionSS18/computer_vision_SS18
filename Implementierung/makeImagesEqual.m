function [imeq1, imeq2] = makeImagesEqual(im1, im2);
%%This function takes two images im1 and im2 and equalize their sizes by
%%adding black rows comlumns to the smaller image

    
%initalizing outputs
    imeq1 = im1;
    imeq2 = im2;

    %getting sizes of images
    [h1, w1, z1] = size (imeq1);
    [h2, w2, z2] = size (imeq2);

    %calculating need rows if any
    extra_coll = floor(abs(w1 - w2)/2);
    extra_colr = ceil(abs(w1 - w2)/2);
    extra_rowu = floor(abs(h1 - h2)/2);
    extra_rowd = ceil(abs(h1 - h2)/2);

    %add stuff to left/right
    if w1 > w2    
        imeq2 = [ zeros(h2,extra_coll,z2) imeq2 zeros(h2,extra_colr,z2)];
        w2 = w1;
    else
        imeq1 = [ zeros(h1,extra_coll,z1) imeq1 zeros(h1,extra_colr,z1)];
        w1 = w2;
    end 

    %add stuff to up/down    
    if h1 > h2
        imeq2 = [ zeros(extra_rowu,w2,z2); imeq2 ; zeros(extra_rowd,w2,z2)];
        h2 = h1;
    else
        imeq1 = [ zeros(extra_rowu,w1,z1); imeq1 ; zeros(extra_rowd,w1,z1)];
        h1 = h2;
    end 
end

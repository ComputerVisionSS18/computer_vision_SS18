function [warped] = warp(img, T)
    [h, w] = size(img);

    warped_coords = zeros(h, w, 3);
    for y = 1:h
        for x = 1:w
            warped_coords(y, x, :) = round([x, y, 1]*T, 0);
        end
    end
    
    % adjust coords so we don't have negative coordinates
    min_warped_x = min(min(warped_coords(:, :, 1)')) - 1;
    min_warped_y = min(min(warped_coords(:, :, 2)')) - 1;

    warped_coords(:, :, 1) = warped_coords(:, :, 1) - ones(size(warped_coords(:, :, 1)))*min_warped_x;
    warped_coords(:, :, 2) = warped_coords(:, :, 2) - ones(size(warped_coords(:, :, 2)))*min_warped_y;
    
    % write new image, could be done with matrix operations as well? 
    for y = 1:h
        for x = 1:w
            warped(warped_coords(y, x, 1), warped_coords(y, x, 2)) = img(y, x);
        end
    end
    warped = flipdim(imrotate(warped, 90),2);
end

function grayImage = rgb_to_gray(coloredImage)
    coloredImage= double(coloredImage);
    img_size_dim = size(size(coloredImage));
    
    % sanity check
    if img_size_dim(2) ~= 3
        grayImage = coloredImage;
    else

        coloredImage(:,:,1) = 0.299 * coloredImage(:,:,1);
        coloredImage(:,:,2) = 0.587 * coloredImage(:,:,2);
        coloredImage(:,:,3) = 0.114 * coloredImage(:,:,3);

        grayImage = sum(coloredImage, 3);
    end

    grayImage = uint8(grayImage);
    
end


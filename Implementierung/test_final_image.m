[img1, img1_gray, img2, img2_gray] = loadData('img/left2.png', 'img/right2.png');

img1_rect = img1_gray;
img2_rect = img2_gray;

% Get K and T
K = get_K();
[E, correspondences] = getEssentialMatrix(img1_gray, img2_gray, K);

% there is no translation and rotation for image 1
T1 = [0; 0; 0];
R1 = eye(3);
[T2, R2] = get_R_and_T_from_E(E,K, correspondences);

% Get disparity map
disparityMap = getDisparity(img1_rect, img2_rect, 21, 31);

% Get depth map
depthMap = getDepthMap(disparityMap, K, T2);

%% Generate virtual image
t_h_v = 0.1; % horizontal translation, only value needed due to rectification
t_h_o = 0;

% Rectify original images

% How do I get the focal distance?
f = K(1,1);

% image centre x, same for virtual and and original view, we can ignore
% them
o_x_v = K(1,3);
o_x_o = K(1,3);


virtual_view = zeros(size(depthMap));
delta_t_h = t_h_v - t_h_o;
% calculate only horizontal shift, vertical shift does not exist due to
% rectification
for y = 1:size(img1, 1) % row index
    for x = 1:size(img1, 2) % column index
        depth = depthMap(y,x);
        new_x = x + f * delta_t_h / depth;
        disp(['old x: ', num2str(x), ' -> new x:', num2str(new_x)]);
    end
end






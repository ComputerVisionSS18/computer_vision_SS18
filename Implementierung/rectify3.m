%% rectify3

function [Tr1,Tr2,Pn1,Pn2, Po1,Po2] = rectify3(K, R2, T2)
    % RECTIFY: compute rectification matrices
    Po2= K*[R2,T2];
    
    R1 = [1 0 0; 0 1 0; 0 0 1];
    T1 = [0, 0, 0]';

    RT1= [R1,T1];
    Po1 = K * RT1;

    % optical centers (unchanged)
    c1 = - inv(Po1(:,1:3))*Po1(:,4);
    c2 = - inv(Po2(:,1:3))*Po2(:,4);

    % new x axis (= direction of the baseline)
    v1 = (c1-c2);

    % new y axes (orthogonal to new x and old z)
    v2 = cross(R1(3, :)',v1);

    % new z axes (orthogonal to baseline and y)
    v3 = R1(3, :)' %cross(v1,v2);
    
    
    % new extrinsic parameters
    R = [v1'/norm(v1)
    v2'/norm(v2)
    v3'/norm(v3)];

    % translation is left unchanged
    % new intrinsic parameters (arbitrary)

    K(1,2)=0; % no skew
    % new projection matrices
    Pn1 = K * [R -R*c1];
    Pn2 = K * [R -R*c2];

    % rectifying image transformation
    Tr1 = (Pn1(1:3,1:3) * inv(Po1(1:3,1:3)))';
    Tr2 = (Pn2(1:3,1:3) * inv(Po2(1:3,1:3)))';
    
    

end

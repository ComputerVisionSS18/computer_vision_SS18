function[depthMap] = getDepthMap(disparityMap, K, T2)
% T2 is the translation of the second image
depthMap = zeros(size(disparityMap));

% Iterate over the disparity map and calculate the depth elementwise
KC = K * T2;
kc = KC(3);
for i = 1:numel(depthMap)
    el = disparityMap(i);
    if el == 0
        depthMap(i) = inf;
    else
        depthMap(i) = kc ./ el;
    end
end

% inf values are intentionally wrong set, so know they can be set to the
% smallest value in the matrix
minVal = min(depthMap(:));
for i = 1:numel(depthMap)
    if depthMap(i) == inf
        depthMap(i) = minVal;
    end
end

end



% function [depthMap] = getDepthMap(img1, img2, K, R, T)
% 
%     [T1, T2, Pn1, Pn2] = rectify(K, R, T)
% 
%     [disparityMap] = getDisparity(T1, T2, img1, img2)
% 
%     rectifiedDepth = getRectifiedDepth(disparityMap)
% 
%     depthMap = derectify(disparity1, disparity2, T1, T2)
% 
% end


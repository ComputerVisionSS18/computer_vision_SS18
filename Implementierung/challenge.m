%% Computer Vision Challenge

% Groupnumber:
group_number = 02;

% Groupmembers:
members = {'Tobias Fuchs', 'Patrick George', 'Jonas Natzer', 'Daniel Padva', 'Sebastian Scheitler'};

% Email-Adress (from Moodle!):
% mail = {'ga99abc@tum.de', 'daten.hannes@tum.de'};
mail = {};

%% Load images


%% Free Viewpoint Rendering

tic;                                            % start execution timer
%Im_out = free_viewpoint(image1, image2, p);    % calculate virtual view
elapsed_time = toc;                             % stop execution timer

%% Display Output

%imshow(Im_out);                                % display virtual view

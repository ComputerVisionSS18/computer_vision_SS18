function [K1, K2] = get_K()
%Funktion baut aus Calib_results.m (wird aus Kalibrierungstoolbox
%generiert)die Kameramatirx K im Format [fx   0  cx;
%                                         0  fy  cy;
%                                         0   0   1: ]
%IN: -
%OUT: K - Kamerakalibirerungsmatrix
%Aufruf: K = get_K

    Calib_Results1;

    K1 = [diag(fc) cc;
         0    0   1];
		 
    Calib_Results2;

    K2 = [diag(fc) cc;
         0    0   1];

end

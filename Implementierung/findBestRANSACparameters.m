%% Find best RANSAC parameters
[img1, img1_gray, img2, img2_gray] = loadData('img/L2.JPG', 'img/R2.JPG');
addpath('./getEssentialMatrixFunctions/');


%% Find Harris features
%           harrisDetector(input_img, segment_length, k, tau, min_dist, tile_size, N, N_gauss, sigma)
features1 = harrisDetector(img1_gray, 15.0, 0.01, 8e5, 25, [200 200], 200, 10, 2);
features2 = harrisDetector(img2_gray, 15.0, 0.01, 8e5, 25, [200 200], 200, 10, 2);

C = getCorrespondenceMatrix(img1_gray,img2_gray,features1,features2, [30 30], 0.95);
robust_C = f_ransac(C, 0.5, 0.2, 0.02);
% showMatchedFeatures(img1, img2, C(1:2, :)', C(3:4, :)', 'montage');
showMatchedFeatures(img1, img2, robust_C(1:2, :)', robust_C(3:4, :)', 'montage');

% Plot
%plot_features(img1, features1);

%% Estimate correspondence
for window_length = 30
    for min_corr = 0.9:0.01:0.99
        for epsilon = [0.4, 0.5, 0.6]
            for p = [0.1, 0.2, 0.3, 0.4]
                for tolerance = [0.01, 0.02, 0.03, 0.04]
                    % getCorrespondenceMatrix(img1, img2, features1, features2, window_length, min_corr);
                    C = getCorrespondenceMatrix(img1,img2,features1,features2, window_length, min_corr);
                    %plot_correspondences(img1, img2, C);

                    % Estimate more robust correspondence points
                    % f_ransac(C, epsilon, p, tolerance)
                    robust_C = f_ransac(C, epsilon, p, tolerance);

                    disp(['Found ', num2str(size(robust_C, 2)), ' correspondencies']);
                    if length(robust_C) >= 10

                        % Plot
                        f = figure();
                        showMatchedFeatures(img1, img2, robust_C(1:2, :)', robust_C(3:4, :)', 'montage');
                        saveas(f, ['ransac_param/corr', num2str(min_corr), 'eps', num2str(epsilon), 'p', num2str(p), 'tol', num2str(tolerance), '.png']);
                        close(f)
                    end
                end
            end
        end
    end
end

% code from [owlbread/MATLAB-stereo-image-disparity-map](https://github.com/owlbread/MATLAB-stereo-image-disparity-map)
% employes [SAD algorithm](https://research.ijcaonline.org/volume107/number3/pxc3899977.pdf)

function [disparityMap] = getDisparity(imgLeft, imgRight, windowSize, sampleSize)
    disp('Generating Disparity Map');
    % takes rectified images img1 and img2

    halfSampleSize = floor(sampleSize/2);

    % image parameters
    [imgHeight, imgWidth] = size(imgLeft);


    disparityMap = zeros(size(imgLeft),'single');
    for row = 1:imgHeight
       rowMin = max(1, row - halfSampleSize);
       rowMax = min(imgHeight, row + halfSampleSize);

       % disp(['Processing row ', num2str(row)]);

       for col = 1:imgWidth
          colMin = max(1, col - halfSampleSize);
          colMax = min(imgWidth, col + halfSampleSize);

          maxNoWindows = min(windowSize, imgWidth - colMax);
          rightWindow = imgRight(rowMin:rowMax , colMin:colMax);
          numBlocks = maxNoWindows + 1;

          differences = zeros(numBlocks,1);
          for i = 0 : maxNoWindows
              leftWindow = imgLeft(rowMin:rowMax, (colMin + i):(colMax + i));
              blockIndex = i + 1;
              differences(blockIndex, 1) = sum(sum(abs(rightWindow - leftWindow)));
          end

          [~, sorted] = sort(differences);
          bestMatchIndex = sorted(1, 1);
          d = bestMatchIndex - 1;

          if ((bestMatchIndex == 1) || (bestMatchIndex == numBlocks))
              disparityMap(row, col) = d;
          else
              C1 = differences(bestMatchIndex - 1);
              C2 = differences(bestMatchIndex);
              C3 = differences(bestMatchIndex + 1);
              disparityMap(row, col) = d - (0.5 * (C3 - C1) / (C1 - (2*C2) + C3));
          end
       end
    end
end

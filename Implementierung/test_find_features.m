[img1, img1_gray, img2, img2_gray] = loadData('img/L1.JPG', 'img/R1.JPG');

K = get_K();

addpath('./getEssentialMatrixFunctions/');

% harrisDetector(input_img, segment_length, k, tau, min_dist, tile_size, N)
features = harrisDetector(img1_gray, 15.0, 0.06, 1e5, 20, [20 20], 200);

% Plot
plot_features(img1_gray, features);





function plot_features(img, features)
    figure;
    x = features(1,:);
    y = features(2,:);
    imshow(uint8(img));
    hold on;
    plot(x,y,'gs', 'MarkerSize', 5);
    hold off;
end


%% T & R bestimmen
function [T,R]=get_R_and_T_from_E(E,K, correspondences)
    % Diese Funktion berechnet die moeglichen Werte fuer T und R
    % aus der Essentiellen Matrix
    
    [U, S, V] = svd(E);
      
    %U and V have to be rotation matrices
    if det(U)<0
        U = U * [1 0 0; 0 1 0; 0 0 -1];
    end
    if det(V)<0
        V = V * [1 0 0; 0 1 0; 0 0 -1];
    end
    %R_z (+/- pi/2)     
    R_plus= [0 -1 0; 1 0 0; 0 0 1];    
    R_minus= [0 1 0; -1 0 0; 0 0 1];   
    
    R1= U* R_plus' * V';
    R2= U* R_minus'* V';
    
    T1_dach = U * R_plus * S * U';
    T2_dach = U * R_minus * S * U';
    
    T1 = [T1_dach(3,2); T1_dach(1,3); T1_dach(2,1)];
    T2 = [T2_dach(3,2); T2_dach(1,3); T2_dach(2,1)];
    
    
    T_cell = {T1, T2, T1, T2};
    R_cell = {R1, R1, R2, R2};

    
    sz = size(correspondences, 2);
    D_init = zeros(sz, 2);
    d_cell = {D_init, D_init, D_init, D_init};
    
    
    
    x1 = correspondences(1:2,:);
    x2 = correspondences(3:4,:);
    einsen = ones(size(correspondences(1,:)));
    
 
    x1_p= [x1; einsen];
    x2_p= [x2; einsen];
    
    x1 = inv(K) * x1_p;
    x2 = inv(K) * x2_p;
    
    N = size(correspondences, 2);
    M1 = zeros(N * 3, N + 1);
    M2 = zeros(N * 3, N + 1);
    M1_cell = {M1, M1, M1, M1};
    M2_cell = {M2, M2, M2, M2};
    
    for i_iter = 1:4
        
        for j_iter = 1:N
           
            M1(j_iter * 3 - 2: j_iter * 3,j_iter) = dach(x2(:,j_iter)) * R_cell{i_iter} * x1(:,j_iter);
            M1(j_iter * 3 - 2: j_iter * 3,N + 1)  = dach(x2(:,j_iter)) * T_cell{i_iter};
            
            M2(j_iter * 3 - 2: j_iter * 3,j_iter) = dach(x1(:,j_iter)) * R_cell{i_iter}' * x2(:,j_iter);
            M2(j_iter * 3 - 2: j_iter * 3,N + 1)  = -dach(x1(:,j_iter)) * R_cell{i_iter}' * T_cell{i_iter};
            
        end
        
        [~,~,V1] = svd(M1);
        [~,~,V2] = svd(M2);       
        
        V1_n = V1(1:end - 1,end) / V1(end,end);
        V2_n = V2(1:end - 1,end) / V2(end,end);         
        
        d_cell{i_iter}(:,1) = V1_n;
        d_cell{i_iter}(:,2) = V2_n;
        
    end
    
    d_cell;
    
    Md1 = cell2mat(d_cell(1));
    Md2 = cell2mat(d_cell(2));
    Md3 = cell2mat(d_cell(3));
    Md4 = cell2mat(d_cell(4));
    
    Md1 = Md1(Md1(:,1) > 0);
    Md2 = Md2(Md2(:,1) > 0);
    Md3 = Md3(Md3(:,1) > 0);
    Md4 = Md4(Md4(:,1) > 0);       
    
    [~,I] = max([size(Md1, 1); size(Md2, 1); size(Md3, 1); size(Md4, 1)]);
    
    T = T_cell{I};
    R = R_cell{I};
    %lambda = d_cell{I};
    
end

function W = dach(w)
  
    W = [0 -w(3) w(2);
        w(3) 0 -w(1);
        -w(2) w(1) 0;];
    
end











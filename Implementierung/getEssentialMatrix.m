
function [E, robust_C] = getEssentialMatrix(img1, img2, K)
    display('Calculate Essential Matrix');
    
    % imgL, imgR and K can be matrices or file paths
    addpath('./getEssentialMatrixFunctions/');


    %% Find Harris features
    %           harrisDetector(input_img, segment_length, k, tau, min_dist, tile_size, N)
    features1 = harrisDetector(img1, 15.0, 0.15, 10e-4, 20, [20 20], 400);
    features2 = harrisDetector(img2, 15.0, 0.15, 10e-4, 20, [20 20], 400);

    % Plot
    %plot_features(img1, features1);
    %plot_features(img2, features2);

    %% Estimate correspondence
    % getCorrespondenceMatrix(img1, img2, features1, features2, window_length, min_corr);
    C = getCorrespondenceMatrix(img1, img2, features1, features2, 21, 0.85);

    corr1 = C(1:2,:)';
    corr2 = C(3:4,:)';

   % figure;showMatchedFeatures(img1, img2, corr1, corr2, 'montage');
    
    
    %corr1 = C(1:2,:)';
    %corr2 = C(3:4,:)';

    %showMatchedFeatures(img1, img2, corr1, corr2, 'montage')
    
    % Estimate more robust correspondence points
    % f_ransac(C, epsilon, p, tolerance)
    robust_C = f_ransac(C, 0.65, 0.95, 5);

    % Plot
    % plot_correspondences(img1, img2, robust_C);
    E = achtPunktAlgorithmus(robust_C, K);
    % save('matrices/E.mat','E');
end



function plot_features(img, features)
    figure;
    x = features(1,:);
    y = features(2,:);
    imshow(uint8(img));
    hold on;
    plot(x,y,'gs', 'MarkerSize', 5);
    hold off;
end

function plot_correspondences(img1, img2, C)
    figure;

    imshow(uint8(img1));
    hold on;
    f = imshow(uint8(img2));
    set(f, 'AlphaData', 0.5);

    % add correspondences/markers
    for cor = C
        x1 = cor(1);
        y1 = cor(2);
        x2 = cor(3);
        y2 = cor(4);

        scatter(x1, y1, 'r');
        scatter(x2, y2, 'g');

        plot([x1, x2], [y1, y2], 'g');

    end
    hold off;
end







%% This is for finding optimal parameters
%% Find best Feature detection parameters
function findBestFeatureDetectionParameters(img1, img2, K)
    % imgL, imgR and K can be matrices or file paths
    addpath('./getEssentialMatrixFunctions/');

    %% Load images and calibration matrix
    [img1, img2, K] = loadData(img1, img2, K);
    % Color to gray
    imgGray1 = rgb_to_gray(img1);

    %% Find Harris features
    for segment_length = [15.0] % does not change much
        for k = [0.01] % lower is better
            for tau = [8e5]
                for min_dist = [25]
                    for N = [200]
                            %           harrisDetector(input_img, segment_length, k, tau, min_dist, tile_size, N)
                            features1 = harrisDetector(imgGray1, segment_length, k, tau, min_dist, [200 200], N);
                            % Plot
                            plot_features(img1, features1);
                    end
                end
            end
        end
    end
end



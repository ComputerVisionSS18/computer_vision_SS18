clc; clear;

[img1, img1_gray, img2, img2_gray] = loadData('img/left2.png', 'img/right2.png');

img1_rect = img1_gray; 
img2_rect = img2_gray;

% Get K and T
K = get_K();
[E, correspondences] = getEssentialMatrix(img1_gray, img2_gray, K);

% there is no translation and rotation for image 1
T1 = [0; 0; 0];
R1 = eye(3);
[T2, R2] = get_R_and_T_from_E(E,K, correspondences);

% Get disparity map
disparityMap = getDisparity(img1_rect, img2_rect, 21, 31);

% Get depth map
depthMap = getDepthMap(disparityMap, K, T2);
% depthMap_norm = (depthMap - min(depthMap(:))) ./ (max(depthMap(:)) - min(depthMap(:)));
% depthMap_img = depthMap_norm .* 255;

% Show results
figure;
%image(depthMap_norm);
imshow(depthMap, [min(depthMap(:)), max(depthMap(:))]);
title('Depth Map (matlab)');
colormap(gca, jet);
colorbar;
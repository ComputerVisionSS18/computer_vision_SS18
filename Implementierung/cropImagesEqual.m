function [imeq1, imeq2] = cropImagesEqual(im1, im2);
%%This function takes two images im1 and im2 and equalize their sizes by
%%adding black rows comlumns to the smaller image

    
%initalizing outputs
    imeq1 = im1;
    imeq2 = im2;

    %getting sizes of images
    [h1, w1, z1] = size (imeq1);
    [h2, w2, z2] = size (imeq2);

    %calculating need rows if any
    extra_coll = ceil(abs(w1 - w2)/2);
    extra_colr = floor(abs(w1 - w2)/2);
    extra_rowu = ceil(abs(h1 - h2)/2);
    extra_rowd = floor(abs(h1 - h2)/2);
    
    %add stuff to left/right
    if w1 > w2    
        imeq1 = imeq1(1:h1,extra_coll:(w1-extra_colr),1:z1);
        w1 = w2;
    else
        imeq2 = imeq2(1:h2,extra_coll:(w2-extra_colr),1:z2);
        w2 = w1;
    end 

    %add stuff to up/down    
    if h1 > h2
        imeq1 = imeq1(extra_rowu:(h1-extra_rowd),1:w1,1:z1);
        h1 = h2;
    else
        imeq2 = imeq2(extra_rowu:(h2-extra_rowd),1:w2,1:z2);
        h2 = h1;
    end 
    
    imeq1 = imeq1(1:h1,1:w1,1:z1);
    imeq2 = imeq2(1:h2,1:w2,1:z2);
    
end

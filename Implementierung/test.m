
clc; clear;

[img1, img1_gray, img2, img2_gray] = loadData('img/L1.JPG', 'img/R1.JPG');

K = get_K();

[E, correspondences] = getEssentialMatrix(img1_gray, img2_gray, K);


% there is no translation and rotaion for image 1
T1 = [0; 0; 0];
R1 = eye(3);
[T2, R2] = get_R_and_T_from_E(E, K, correspondences);


Tx2 = -R2'*T2;
Rx2 = R2';


[Tr1, Tr2, Pn1, Pn2, Po1, Po2] = rectify3(K, Rx2, Tx2);
img1_rect = imwarp(img1_gray, projective2d(Tr2));
img2_rect = imwarp(img2_gray, projective2d(Tr1));


size(img1_rect)
size(img2_rect)

figure;
imshow(img1_rect);
figure;
imshow(img2_rect);

% 
% [img1_rect, img2_rect] = makeImagesEqual(img1_rect,img2_rect);
% 
% disparityMap = disparity(img1_rect, img2_rect);
% figure;
% imshow(disparityMap, [-6, 10]);
% title('Disp Map (matlab)');
% colormap(gca, jet);
% colorbar;
% 
% disparityMap = getDisparity(img1_rect, img2_rect);
% figure;
% imshow(disparityMap, [-6, 10]);
% title('Disp Map');
% colormap(gca, jet);
% colorbar;
% 


% get Disparity(img1, img2, windowSize, sampleSize)
%disparityMap = getDisparity(imeq1, imeq2, 25, 15);
%figure;
%imshow(disparityMap);
%title('Disp Map (own)');
%colormap(gca, jet);
%colorbar;

%corr1 = correspondences(1:2,:)';
%corr2 = correspondences(3:4,:)';

%figure;showMatchedFeatures(img1, img2, corr1, corr2, 'montage');

%f = estimateFundamentalMatrix(corr1,corr2,...
    %'Method','Norm8Point');

%[t1, t2] = estimateUncalibratedRectification(f,corr1,...
    %corr2,size(img1));

%I1Rect = imwarp(img1, projective2d(t1));
%I2Rect = imwarp(img2, projective2d(t2));


%figure; imshow(imeq1);
%figure; imshow(imeq2);

%disparityMap = getDisparity(imeq1, imeq2);
%imshow(disparityMap,[-6 10]);

%

%
%
% R1 = eye(3, 3);
% t1 = [0, 0, 0]';
% [t2, R2] =  get_R_and_T_from_E(E, K, correspondences);
%
% Po1 = K * [R1, t1];
% Po2 = K * [R2, t2];
%
% [T1, T2, Pn1, Pn2] = rectify(Po1, Po2);
%
% J1 = imwarp(img1,projective2d(T1));
% J2 = imwarp(img2,projective2d(T2));
%
% t2
% rotm2axang(R2)
%
% figure; imshow(J1)
% figure; imshow(J2)
%
%


%depthMap = getDepthMap(I1, I2, K, R, T)

% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3129.42627 ; 3129.42627 ];

%-- Principal point:
cc = [ 1499.500000000000000 ; 999.500000000000000 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ -0.138825253737073 ; 0.209802354741939 ; 0.009758220769515 ; 0.017633866600616 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 436.552127231196550 ; 434.278168842903654 ];

%-- Principal point uncertainty:
cc_error = [ 0.000000000000000 ; 0.000000000000000 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.096359358978224 ; 0.176866348462096 ; 0.003515463715539 ; 0.014447716068888 ; 0.000000000000000 ];

%-- Image size:
nx = 3000;
ny = 2000;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 3;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 0;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 1.834003e+00 ; 2.512396e+00 ; 1.962900e-02 ];
Tc_1  = [ -5.777710e+01 ; -3.309087e+02 ; 1.155879e+03 ];
omc_error_1 = [ 3.186337e-02 ; 5.480245e-02 ; 8.884078e-02 ];
Tc_error_1  = [ 1.311206e+00 ; 1.766477e+00 ; 2.131811e+02 ];

%-- Image #2:
omc_2 = [ -2.155078e+00 ; -2.178772e+00 ; -1.513269e-02 ];
Tc_2  = [ -3.907230e+01 ; -6.162854e+01 ; 7.529049e+02 ];
omc_error_2 = [ 2.849770e-02 ; 3.131354e-02 ; 5.718744e-02 ];
Tc_error_2  = [ 2.863228e-01 ; 3.189158e-01 ; 1.299705e+02 ];

%-- Image #3:
omc_3 = [ 1.959415e+00 ; 1.938439e+00 ; 5.744815e-01 ];
Tc_3  = [ -3.660701e+02 ; -6.570829e+01 ; 7.193061e+02 ];
omc_error_3 = [ 2.386702e-02 ; 2.735931e-02 ; 4.308472e-02 ];
Tc_error_3  = [ 5.190443e+00 ; 9.838634e-01 ; 1.458871e+02 ];


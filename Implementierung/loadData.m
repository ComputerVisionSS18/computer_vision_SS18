% Input data can be a matrix or a filepath, this function loads data from
% files as necessary
% gray images are doubles
function [img1, img1_gray, img2, img2_gray] = loadData(img1, img2)
    if ischar(img1)
        img1 = imread(img1);
        img1_gray = rgb_to_gray(img1);
    end
    if ischar(img2)
        img2 = imread(img2);
        img2_gray = rgb_to_gray(img2);
    end
end
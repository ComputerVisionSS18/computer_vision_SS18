% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3881.281051929638579 ; 3884.622940078799274 ];

%-- Principal point:
cc = [ 1499.500000000000000 ; 999.500000000000000 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.054477108116770 ; -0.181080461255223 ; 0.003135788370950 ; 0.000871477889946 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 66.109795924026912 ; 65.384676560265248 ];

%-- Principal point uncertainty:
cc_error = [ 0.000000000000000 ; 0.000000000000000 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.037828898828522 ; 0.400687457601706 ; 0.001736864975577 ; 0.001737696063616 ; 0.000000000000000 ];

%-- Image size:
nx = 3000;
ny = 2000;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 3;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 0;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 2.171404e+00 ; 2.222325e+00 ; 1.901931e-01 ];
Tc_1  = [ -1.290850e+02 ; -7.265490e+01 ; 6.044971e+02 ];
omc_error_1 = [ 3.912517e-03 ; 4.236857e-03 ; 6.750012e-03 ];
Tc_error_1  = [ 1.021882e-01 ; 6.853916e-02 ; 1.043664e+01 ];

%-- Image #2:
omc_2 = [ 1.999825e+00 ; 2.095925e+00 ; 4.922201e-01 ];
Tc_2  = [ -6.365393e+01 ; -9.656534e+01 ; 6.136351e+02 ];
omc_error_2 = [ 2.139279e-03 ; 2.255821e-03 ; 2.923128e-03 ];
Tc_error_2  = [ 9.780523e-02 ; 7.218769e-02 ; 1.077263e+01 ];

%-- Image #3:
omc_3 = [ -2.052879e+00 ; -2.186776e+00 ; -2.754283e-02 ];
Tc_3  = [ -1.467824e+02 ; -1.060240e+02 ; 5.909281e+02 ];
omc_error_3 = [ 2.966816e-03 ; 3.009621e-03 ; 7.139529e-03 ];
Tc_error_3  = [ 6.217078e-02 ; 6.515646e-02 ; 1.023455e+01 ];


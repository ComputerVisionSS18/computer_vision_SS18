
clc; clear;

[img1, img1_gray, img2, img2_gray] = loadData('img/left2.png', 'img/right2.png');

%K = get_K();

%[E, correspondences] = getEssentialMatrix(img1_gray, img2_gray, K);


% there is no translation and rotaion for image 1
%T1 = [0; 0; 0];
%R1 = eye(3);
%[T2, R2] = get_R_and_T_from_E(E, K, correspondences);


%Tx2 = -R2'*T2;
%Rx2 = R2';


%[Tr1, Tr2, Pn1, Pn2, Po1, Po2] = rectify3(K, Rx2, Tx2);
%img1_rect = imwarp(img1_gray, projective2d(Tr2));
%img2_rect = imwarp(img2_gray, projective2d(Tr1));


%size(img1_rect)
%size(img2_rect)
 
 %figure;
 %imshow(img1_rect);
 %figure;
 %imshow(img2_rect);

%[img1_rect, img2_rect] = cropImagesEqual(img1_rect, img2_rect);

% figure;
% imshow(img1_rect);
% figure;
% imshow(img2_rect);


%[img1_rect, img2_rect] = makeImagesEqual(img1_rect,img2_rect);

img1_rect = img1_gray; 
img2_rect = img2_gray;

disparityMap = getDisparity(img1_rect, img2_rect, 21, 31);
figure;
imshow(disparityMap, [0, max(disparityMap(:))]);
title('Disp Map (matlab)');
colormap(gca, jet);
colorbar;


